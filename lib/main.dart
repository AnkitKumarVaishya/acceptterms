import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Accepting Terms And License",
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool checkbox = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Learning About Accepting Terms")),
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(
          "Accept Terms",
          style: TextStyle(color: Colors.green, fontSize: 20.00),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Checkbox(
                value: checkbox,
                onChanged: (bool value) {
                                  setState(() {
                    checkbox = value;
                   
                  });
                   print(checkbox);
                },
                
              ),
              Text("Are You Sure Want To Accept Our Terms?"),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: checkbox ? displaymessage : null,
              child: Text("Continue"),
            ),
          ],
        ),
      ]),
    );
  }

  void displaymessage() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        AlertDialog dialog = AlertDialog(
          content: Text("You Have Accepted The Terms."),
          actions: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text("OK"),
            ),
          ],
        );
        return dialog;
      },
    );
  }
}
